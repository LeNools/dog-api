**node** 12.14.1
**npm** 6.14.2

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

### How to start

**npm install**<br />
**npm start**

### Packages

**react-router-dom** for navigation purpose to easy navigate between screens and pass data between them.<br />
**axios** for making XMLHttpRequests easier.<br />
**Material-UI** for faster and easier web development by adding pre made components.

### How it works

You will start by liking different pictures of dogs. Once you liked a breed three times you will be redirected to path /breed/{breed}. There you will get a random picture of that breed and a text telling the user that this is probably the breed they like. From there you can navigate back to restart the process.

I used "useEffect" to fetch data and also to be able to trigger the same function again when pressing load more. I made it this way so you can decide on how many dogs to fetch and fetch more when necessary. This will make the site run faster.<br />

I used "setState" to handle the data being fetched with states like "loading, error, data". This was also used to keep track of what breeds where liked by the user.<br />

I took the breed out from the url that was fetched from the api and used the picture name(without endpoint) and index to make a unique key for the dog cards.
