import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Container } from "@material-ui/core";
import Breeds from "./pages/breeds";
import Breed from "./pages/breed";

function App() {
  return (
    <Router>
      <Container>
        <Switch>
          <Route exact path="/breed/:breed" component={Breed} />
          <Route path="/" component={Breeds} />
        </Switch>
      </Container>
    </Router>
  );
}

export default App;
