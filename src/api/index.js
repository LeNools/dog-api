import axios from "axios";

const BASE_URL = "https://dog.ceo/api/";

export const axiosGet = endpoint => {
  return axios
    .get(`${BASE_URL + endpoint}`)
    .then(({ data }) => {
      return data.message;
    })
    .catch(error => {
      return error;
    });
};
