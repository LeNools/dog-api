import React, { useEffect, useState } from "react";
import { axiosGet } from "../../api";
import { Link } from "react-router-dom";
import { Box, Typography, makeStyles } from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    display: "flex",
    flexDirection: "row",
    flex: "wrap",
    width: "100%"
  },
  image: {
    maxWidth: 400,
    maxHeight: 400,
    width: "100%",
    height: "auto"
  },
  text: {
    maxWidth: 400,
    marginBottom: theme.spacing(2)
  },
  link: {
    textDecoration: "none",
    color: "#000"
  }
}));

const fetchRandomBreed = async (setState, state, breed) => {
  const END_POINT = `breed/${breed}/images/random`;

  setState({
    ...state,
    loading: true
  });

  try {
    const url = await axiosGet(END_POINT);
    setState({
      ...state,
      loading: false,
      error: false,
      data: url
    });
  } catch (error) {
    setState({
      ...state,
      loading: false,
      error: true
    });
  }
};

const Breed = ({ match: { params } }) => {
  const classes = useStyles();
  const { breed } = params;
  const [breedUrl, setUrl] = useState({
    loading: false,
    error: false,
    data: ""
  });
  const { data, error, loading } = breedUrl;

  useEffect(() => {
    fetchRandomBreed(setUrl, breedUrl, breed);
  }, [breed]);

  if (loading) {
    return <p>Loading...</p>;
  }

  if (error) {
    return <p>Error has occurred</p>;
  }

  return (
    <Box display="flex" flexDirection="column" alignItems="center">
      <Box>
        <Typography variant="h1">{breed}</Typography>
        <img src={data} alt={breed} className={classes.image} />
        <Typography
          className={classes.text}
        >{`Seems that you like the breed ${breed}. This breed was liked three times by you! `}</Typography>
        <Link to="/" className={classes.link}>
          <Box display="flex" flexDirection="row" alignItems="center">
            <ArrowBackIcon />
            <Typography variant="h6">Go Back</Typography>
          </Box>
        </Link>
      </Box>
    </Box>
  );
};

export default Breed;
