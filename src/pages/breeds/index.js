import React, { useEffect, useState, Fragment } from "react";
import { axiosGet } from "../../api";
import {
  Box,
  IconButton,
  CircularProgress,
  Button,
  makeStyles
} from "@material-ui/core";
import ThumbUpIcon from "@material-ui/icons/ThumbUp";
import Card from "./components/Card";

const useStyles = makeStyles(theme => ({
  loader: {
    alignSelf: "center"
  },
  loadMore: {
    textAlign: "center"
  }
}));

const fetchRandomBreeds = async (setState, state) => {
  const NUMBER_OF_IMAGES = 10;
  const END_POINT = `breeds/image/random/${NUMBER_OF_IMAGES}`;

  setState({
    ...state,
    loading: true
  });

  try {
    const breeds = await axiosGet(END_POINT);
    setState({
      ...state,
      loading: false,
      error: false,
      data: [...state.data, ...breeds]
    });
  } catch (error) {
    setState({
      ...state,
      loading: false,
      error: true
    });
  }
};

const BreedCards = ({ urls, like, likedBreeds }) => {
  return urls.map((url, index) => {
    const urlData = url.split("/").reverse();
    // image name(without type of file) + index
    const key = urlData[0].substr(0, urlData[0].indexOf(".")) + index;
    // breed without sub breed
    const breedSplitIndex = urlData[1].indexOf("-");
    const breed =
      breedSplitIndex < 0 ? urlData[1] : urlData[1].substr(0, breedSplitIndex);

    const likedBreed = likedBreeds?.[breed] || [];
    const isLiked = likedBreed.includes(key);

    return (
      <Card key={key} url={url}>
        <IconButton
          onClick={() => like(breed, key)}
          color={isLiked ? "primary" : "default"}
        >
          <ThumbUpIcon />
        </IconButton>
      </Card>
    );
  });
};

const like = (setState, state, history) => (breed, key) => {
  const tmpLikes = state?.[breed] ? [...state[breed]] : [];
  // check if already liked. Will return -1 if not found
  const index = tmpLikes.indexOf(key);

  if (index < 0) {
    if (tmpLikes.length < 2) {
      setState({
        ...state,
        [breed]: [...tmpLikes, key]
      });
    } else {
      history.push(`/breed/${breed}`);
    }
  } else {
    tmpLikes.splice(index, 1);
    setState({
      ...state,
      [breed]: tmpLikes
    });
  }
};

const Breeds = ({ history }) => {
  const classes = useStyles();
  const [likedBreeds, likeBreed] = useState({});
  const [breeds, setBreeds] = useState({
    loading: false,
    error: false,
    data: [],
    increment: 0
  });

  const { loading, error, data, increment } = breeds;
  useEffect(() => {
    fetchRandomBreeds(setBreeds, breeds);
  }, [increment]);

  if (error) {
    return <p>Error has occurred</p>;
  }

  return (
    <Box display="flex" flexDirection="column">
      {data.length > 0 && (
        <Fragment>
          <Box
            display="flex"
            flexDirection="row"
            flexWrap="wrap"
            justifyContent="center"
          >
            <BreedCards
              urls={data}
              like={like(likeBreed, likedBreeds, history)}
              likedBreeds={likedBreeds}
            />
          </Box>
          <Button
            className={classes.loadMore}
            onClick={() =>
              setBreeds({ ...breeds, increment: breeds.increment + 1 })
            }
          >
            Load more
          </Button>
        </Fragment>
      )}
      {loading && <CircularProgress size={50} className={classes.loader} />}
    </Box>
  );
};

export default Breeds;
