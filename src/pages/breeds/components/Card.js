import React from "react";
import { string, element } from "prop-types";
import {
  Card as MuiCard,
  CardMedia as MuiCardMedia,
  CardActions as MuiCardActions,
  makeStyles
} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  root: {
    margin: theme.spacing(2)
  },
  cardImage: {
    marginTop: theme.spacing(3),
    height: 400,
    width: 400
  }
}));

const Card = ({ url, children }) => {
  const classes = useStyles();

  return (
    <MuiCard className={classes.root}>
      <MuiCardMedia title="" image={url} className={classes.cardImage} />
      <MuiCardActions>{children}</MuiCardActions>
    </MuiCard>
  );
};

Card.propTypes = {
  url: string.isRequired,
  children: element
};

export default Card;
